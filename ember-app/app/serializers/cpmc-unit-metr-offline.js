import { OfflineSerializer as UnitMetrSerializer } from
  '../mixins/regenerated/serializers/cpmc-unit-metr-offline';
import __ApplicationSerializer from './application-offline';

export default __ApplicationSerializer.extend(UnitMetrSerializer, {
});
