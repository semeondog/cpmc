import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function () {
  this.route('cpmc-d-s-e-l');
  this.route('cpmc-d-s-e-e',
  { path: 'cpmc-d-s-e-e/:id' });
  this.route('cpmc-d-s-e-e.new',
  { path: 'cpmc-d-s-e-e/new' });
  this.route('cpmc-machine-l');
  this.route('cpmc-machine-e',
  { path: 'cpmc-machine-e/:id' });
  this.route('cpmc-machine-e.new',
  { path: 'cpmc-machine-e/new' });
  this.route('cpmc-material-l');
  this.route('cpmc-material-e',
  { path: 'cpmc-material-e/:id' });
  this.route('cpmc-material-e.new',
  { path: 'cpmc-material-e/new' });
  this.route('cpmc-operation-l');
  this.route('cpmc-operation-e',
  { path: 'cpmc-operation-e/:id' });
  this.route('cpmc-operation-e.new',
  { path: 'cpmc-operation-e/new' });
  this.route('cpmc-plan-l');
  this.route('cpmc-plan-e',
  { path: 'cpmc-plan-e/:id' });
  this.route('cpmc-plan-e.new',
  { path: 'cpmc-plan-e/new' });
  this.route('cpmc-process-order-l');
  this.route('cpmc-process-order-e',
  { path: 'cpmc-process-order-e/:id' });
  this.route('cpmc-process-order-e.new',
  { path: 'cpmc-process-order-e/new' });
  this.route('cpmc-tool-l');
  this.route('cpmc-tool-e',
  { path: 'cpmc-tool-e/:id' });
  this.route('cpmc-tool-e.new',
  { path: 'cpmc-tool-e/new' });
  this.route('cpmc-unit-metr-l');
  this.route('cpmc-unit-metr-e',
  { path: 'cpmc-unit-metr-e/:id' });
  this.route('cpmc-unit-metr-e.new',
  { path: 'cpmc-unit-metr-e/new' });
  this.route('cpmc-work-type-l');
  this.route('cpmc-work-type-e',
  { path: 'cpmc-work-type-e/:id' });
  this.route('cpmc-work-type-e.new',
  { path: 'cpmc-work-type-e/new' });
  this.route('login');

  // Аудит.
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-access-l');
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-access-e', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-access-e/:id' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-access-e.new', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-access-e/new' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-class-l');
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-class-e', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-class-e/:id' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-class-e.new', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-class-e/new' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-group-l');
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-group-e', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-group-e/:id' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-group-e.new', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-group-e/new' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-link-group-l');
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-link-group-e', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-link-group-e/:id' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-link-group-e.new', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-link-group-e/new' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-link-role-l');
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-link-role-e', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-link-role-e/:id' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-link-role-e.new', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-link-role-e/new' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-operation-l');
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-operation-e', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-operation-e/:id' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-operation-e.new', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-operation-e/new' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-permition-l');
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-permition-e', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-permition-e/:id' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-permition-e.new', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-permition-e/new' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-role-l');
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-role-e', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-role-e/:id' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-role-e.new', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-role-e/new' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-user-l');
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-user-e', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-user-e/:id' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-user-e.new', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-user-e/new' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-view-l');
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-view-e', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-view-e/:id' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-security-view-e.new', { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-security-view-e/new' });

  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-business-audit-objects-audit-entity-l');
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-business-audit-objects-audit-entity-e',
    { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-business-audit-objects-audit-entity-e/:id' });
  this.route('i-c-s-soft-s-t-o-r-m-n-e-t-business-audit-objects-audit-entity-e.new',
    { path: 'i-c-s-soft-s-t-o-r-m-n-e-t-business-audit-objects-audit-entity-e/new' });

});

export default Router;
