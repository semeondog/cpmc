import { OfflineSerializer as ProcessOrderItemSerializer } from
  '../mixins/regenerated/serializers/cpmc-process-order-item-offline';
import __ApplicationSerializer from './application-offline';

export default __ApplicationSerializer.extend(ProcessOrderItemSerializer, {
});
