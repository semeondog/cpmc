import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('cpmc-gantt-chart', 'Integration | Component | cpmc gantt chart', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{cpmc-gantt-chart}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#cpmc-gantt-chart}}
      template block text
    {{/cpmc-gantt-chart}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
