import { moduleForModel, test } from 'ember-qunit';

moduleForModel('cpmc-unit-metr', 'Unit | Serializer | cpmc-unit-metr', {
  // Specify the other units that are required for this test.
  needs: [
    'serializer:cpmc-unit-metr',
    'transform:file',
    'transform:decimal',
    'transform:guid',

    'transform:cpmc-t-plan-algorithm',
    'transform:cpmc-t-plan-state',

    'model:cpmc-d-s-e-rout',
    'model:cpmc-d-s-e',
    'model:cpmc-machine',
    'model:cpmc-material',
    'model:cpmc-operation',
    'model:cpmc-plan-item',
    'model:cpmc-plan',
    'model:cpmc-process-order-item',
    'model:cpmc-process-order',
    'model:cpmc-resourse',
    'model:cpmc-tool',
    'model:cpmc-unit-metr',
    'model:cpmc-work-type'
  ]
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  let record = this.subject();

  let serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
