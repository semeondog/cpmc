import { OfflineSerializer as WorkTypeSerializer } from
  '../mixins/regenerated/serializers/cpmc-work-type-offline';
import __ApplicationSerializer from './application-offline';

export default __ApplicationSerializer.extend(WorkTypeSerializer, {
});
