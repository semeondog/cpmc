import { Serializer as ProcessOrderItemSerializer } from
  '../mixins/regenerated/serializers/cpmc-process-order-item';
import __ApplicationSerializer from './application';

export default __ApplicationSerializer.extend(ProcessOrderItemSerializer, {
  /**
  * Field name where object identifier is kept.
  */
  primaryKey: '__PrimaryKey'
});
