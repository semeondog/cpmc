import { OfflineSerializer as ToolSerializer } from
  '../mixins/regenerated/serializers/cpmc-tool-offline';
import ResourseSerializer from './cpmc-resourse-offline';

export default ResourseSerializer.extend(ToolSerializer, {
});
