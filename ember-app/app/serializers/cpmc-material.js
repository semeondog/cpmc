import { Serializer as MaterialSerializer } from
  '../mixins/regenerated/serializers/cpmc-material';
import ResourseSerializer from './cpmc-resourse';

export default ResourseSerializer.extend(MaterialSerializer, {
  /**
  * Field name where object identifier is kept.
  */
  primaryKey: '__PrimaryKey'
});
