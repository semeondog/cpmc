import { moduleForModel, test } from 'ember-qunit';

moduleForModel('cpmc-process-order-item', 'Unit | Model | cpmc-process-order-item', {
  // Specify the other units that are required for this test.
  needs: [
    'model:cpmc-d-s-e-rout',
    'model:cpmc-d-s-e',
    'model:cpmc-machine',
    'model:cpmc-material',
    'model:cpmc-operation',
    'model:cpmc-plan-item',
    'model:cpmc-plan',
    'model:cpmc-process-order-item',
    'model:cpmc-process-order',
    'model:cpmc-resourse',
    'model:cpmc-tool',
    'model:cpmc-unit-metr',
    'model:cpmc-work-type'
  ]
});

test('it exists', function(assert) {
  let model = this.subject();

  // let store = this.store();
  assert.ok(!!model);
});
