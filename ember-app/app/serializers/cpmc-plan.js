import { Serializer as PlanSerializer } from
  '../mixins/regenerated/serializers/cpmc-plan';
import __ApplicationSerializer from './application';

export default __ApplicationSerializer.extend(PlanSerializer, {
  /**
  * Field name where object identifier is kept.
  */
  primaryKey: '__PrimaryKey'
});
