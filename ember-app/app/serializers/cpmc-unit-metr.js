import { Serializer as UnitMetrSerializer } from
  '../mixins/regenerated/serializers/cpmc-unit-metr';
import __ApplicationSerializer from './application';

export default __ApplicationSerializer.extend(UnitMetrSerializer, {
  /**
  * Field name where object identifier is kept.
  */
  primaryKey: '__PrimaryKey'
});
