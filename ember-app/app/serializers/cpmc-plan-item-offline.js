import { OfflineSerializer as PlanItemSerializer } from
  '../mixins/regenerated/serializers/cpmc-plan-item-offline';
import __ApplicationSerializer from './application-offline';

export default __ApplicationSerializer.extend(PlanItemSerializer, {
});
