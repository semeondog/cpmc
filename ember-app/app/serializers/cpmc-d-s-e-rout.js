import { Serializer as DSERoutSerializer } from
  '../mixins/regenerated/serializers/cpmc-d-s-e-rout';
import __ApplicationSerializer from './application';

export default __ApplicationSerializer.extend(DSERoutSerializer, {
  /**
  * Field name where object identifier is kept.
  */
  primaryKey: '__PrimaryKey'
});
