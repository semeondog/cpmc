import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('cpmc-print-plan', 'Integration | Component | cpmc print plan', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{cpmc-print-plan}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#cpmc-print-plan}}
      template block text
    {{/cpmc-print-plan}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
