import { Serializer as ResourseSerializer } from
  '../mixins/regenerated/serializers/cpmc-resourse';
import __ApplicationSerializer from './application';

export default __ApplicationSerializer.extend(ResourseSerializer, {
  /**
  * Field name where object identifier is kept.
  */
  primaryKey: '__PrimaryKey'
});
